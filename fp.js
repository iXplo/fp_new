class Message {
  constructor(initObject) {
    Object.assign(this, initObject);
  }

  catchError(error) {
    console.warn(error);
    this.state.change('error', {error});
  }

  load({id, key}) {
    //todo
    // return fetch(`${this.apiUrl}/${id}`, {cache: 'no-cache'})
    return fetch(`fakeLoad`)
      .then(response => {
        return response.json();
      })
      .then(parsed => {
        let {message, images} = this.decode({
          message: parsed.message,
          images: parsed.images || [],
          key
        });
        this.state.change('read', {message, images})
      })
      .catch(this.catchError);
  }

  save() {
    //todo
    let {message, images, key} = this.encode({
      message: this.body,
      images: this.images
    });

    let objectToSend = {message};
    if (images.length) {
      objectToSend.images = images;
    }
    // return fetch(this.apiUrl, {
    return fetch('fakeSave', {
      method: 'post',
      cache: 'no-cache',
      body: objectToSend
    })
      .then(response => {
        return response.json();
      })
      .then(parsed => {
        this.state.change('link', {id: parsed.id, key});
      })
      .catch(this.catchError);
  }

  encode({message, images}) {
    //todo
    let key = 'randomGeneratedSecretKey';
    return {message, images, key};
  }

  decode({message, images, key}) {
    //todo
    return {message, images};
  }

  get body() {
    return this._body;
  }

  set body(newValue) {
    console.log(newValue);
    this._body = newValue;
  }
}

class StateController {
  constructor() {
    let query = document.querySelector.bind(document);
    this.imagesBlock = query('#images');
    this.messageInput = query('#message');
    this.states = {
      read: this.readState,
      write: this.writeState,
      link: this.linkState,
      error: this.errorState
    };
  }

  change(state, paramsObject) {
    let funcToRun = this.states[state].bind(this);
    funcToRun(paramsObject);
  }

  readState({message, images = []}) {
    this.messageInput.value = message;

    if (images.length) {
      let imgs = document.createDocumentFragment();

      for (let image of images) {
        let img = document.createElement("img");
        img.src = image;
        imgs.appendChild(img);
      }

      this.imagesBlock.appendChild(imgs);
    }
  }

  writeState() {
    this.messageInput.value = '';
  }
}

class Init {
  constructor() {
    let id = location.search.substr(1).trim();
    let key = location.search.substr(1).trim();

    let stateController = this.stateController = new StateController();
    let message = this.message = new Message({
      apiUrl: '/api',
      state: stateController
    });

    if (id) {
      message.load({id, key});
    } else {
      stateController.change('write');
    }
  }
}


document.addEventListener("DOMContentLoaded", () => {
  window.fp = new Init();
});